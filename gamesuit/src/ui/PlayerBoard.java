package ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ColorConvertOp;
import java.util.ArrayList;

import javax.swing.*;

import domain.model.DiceFactory;
import domain.model.PuntenCategorie;
import domain.model.Speler;
import ui.listeners.ButtonGooitDobbelstenenListener;
import ui.listeners.ButtonSpeelDobbelstenenListener;
import ui.listeners.ButtonVoegSpelersToeListener;

public class PlayerBoard extends JFrame implements Observer {
	private ControllerYatzee controller;
	private int worpen = 2;
	private JPanel dobbelstenen;
	private Speler speler;
	private JPanel overal = new JPanel();
	private JLabel dobbelsteen1 , dobbelsteen2, dobbelsteen3, dobbelsteen4, dobbelsteen5 ;
	private JRadioButton dobut1 , dobut2, dobut3,dobut4,dobut5;
	private JButton gooi, speel;
	private JLabel eenl ,tweel, driel , vierl , vijfl , zesl , dezelfde3l, dezelfde4l, fullhousel, kleinel ,grotel ,yatzeel ,chancel;
	private JComboBox<PuntenCategorie> categorie�n;
	private JPanel knoppen;
	
	public PlayerBoard(Speler speler , ControllerYatzee controller){
		this.controller = controller;
		this.speler = speler;
	}
	public void drawPlayerBoard(){
		
		this.setSize(420,450);
		//this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);	
		this.setTitle(speler.getNaam()); // heir moet de spelernaam komen (te verkrijgen via de controller)
		
		
		JPanel spelinformatie = new JPanel();
		JPanel punten = this.punten(speler);
		dobbelstenen = this.dobbelstenen();
		knoppen = knopen();
		
		JLabel spelinformatoe = new JLabel("veel succes " + speler.getNaam());
		Font font = new Font(Font.SANS_SERIF, Font.BOLD, 20);
		spelinformatoe.setFont(font);
		spelinformatie.add(spelinformatoe);
		
		
		overal.add(spelinformatie);
		overal.add(punten);
		overal.add(dobbelstenen);
		overal.add(knoppen);
		//this.setResizable(false);
		this.add(overal);
		this.changeToWaiteState();
		this.setVisible(true);
	}
	
	@Override
	public void updateDobbelstenen() {
		ImageIcon image;
		worpen--;
		if(dobut1.isSelected() == false){
			image = DiceFactory.SetNewImage();
			dobbelsteen1.setIcon(image);
			dobbelsteen1.setToolTipText(image.getDescription());
		}
		if(dobut2.isSelected()== false){
			image = DiceFactory.SetNewImage();
			dobbelsteen2.setIcon(image);
			dobbelsteen2.setToolTipText(image.getDescription());
		}
		if(dobut3.isSelected()== false){
			image = DiceFactory.SetNewImage();
			dobbelsteen3.setIcon(image);
			dobbelsteen3.setToolTipText(image.getDescription());
		}
		if(dobut4.isSelected()== false){
			image = DiceFactory.SetNewImage();
			dobbelsteen4.setIcon(image);
			dobbelsteen4.setToolTipText(image.getDescription());
		}
		if(dobut5.isSelected()== false){
			image = DiceFactory.SetNewImage();
			dobbelsteen5.setIcon(image);
			dobbelsteen5.setToolTipText(image.getDescription());
		}
		if(worpen == 0){
			gooi.setEnabled(false);
		}
		
	}
	@Override
	public void updateScore() {
		ArrayList<String> dobbelTooltip = new ArrayList<>();
		dobbelTooltip.add(dobbelsteen1.getToolTipText());
		dobbelTooltip.add(dobbelsteen2.getToolTipText());
		dobbelTooltip.add(dobbelsteen3.getToolTipText());
		dobbelTooltip.add(dobbelsteen4.getToolTipText());
		dobbelTooltip.add(dobbelsteen5.getToolTipText());
		speler.berekenScore(dobbelTooltip, (PuntenCategorie) categorie�n.getSelectedItem());
		
	}

	
	public JPanel knopen(){
		JPanel knoppen = new JPanel();
		gooi = new JButton("gooi dobbelstenen");
		ButtonGooitDobbelstenenListener buttongooidobbelstenenlistener = new ButtonGooitDobbelstenenListener(this);
		gooi.addActionListener(buttongooidobbelstenenlistener);
		speel = new JButton("speel dobbelstenen");
		ButtonSpeelDobbelstenenListener buttonspeeldobbelstenenlistener = new ButtonSpeelDobbelstenenListener(this , controller);
		speel.addActionListener(buttonspeeldobbelstenenlistener);
		categorie�n = new JComboBox<PuntenCategorie>(PuntenCategorie.values());
		
		knoppen.add(gooi);
		knoppen.add(categorie�n);
		knoppen.add(speel);
		
	
		return knoppen;
	}
	
	public JPanel dobbelstenen() {
		JPanel dobbelstenen = new JPanel(new GridBagLayout());
		
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx =0;
		c.gridy= 0;
		//c.fill = GridBagConstraints.BOTH;
	
		dobbelsteen1 = DiceFactory.makeDobbelsteen();
		dobbelstenen.add(dobbelsteen1 ,c);
		c.gridx= 1;
		dobbelsteen2 = DiceFactory.makeDobbelsteen();
		dobbelstenen.add(dobbelsteen2 ,c);
		c.gridx= 2;
		dobbelsteen3 = DiceFactory.makeDobbelsteen();
		dobbelstenen.add(dobbelsteen3 ,c);
		c.gridx= 3;
		dobbelsteen4 = DiceFactory.makeDobbelsteen();
		dobbelstenen.add(dobbelsteen4 ,c);
		c.gridx= 4;
		dobbelsteen5 =DiceFactory.makeDobbelsteen();
		dobbelstenen.add(dobbelsteen5 ,c);
		
		c.gridy = 1;
		c.gridx = 0;
		dobut1 = new JRadioButton("houden");
		dobbelstenen.add(dobut1,c);
		c.gridx = 1;
		dobut2 = new JRadioButton("houden");
		dobbelstenen.add(dobut2,c);
		c.gridx = 2;
		 dobut3 = new JRadioButton("houden");
		dobbelstenen.add(dobut3,c);
		c.gridx = 3;
		dobut4 = new JRadioButton("houden");
		dobbelstenen.add(dobut4,c);
		c.gridx = 4;
		dobut5 = new JRadioButton("houden");
		dobbelstenen.add(dobut5,c);
		
		return dobbelstenen;
	}
	
	public JPanel punten(Speler speler){
		JPanel punten = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx =0;
		c.gridy= 0;
		//c.gridwidth = c.gridheight = 1;
		c.fill = GridBagConstraints.BOTH;
		//c.anchor = GridBagConstraints.NORTHWEST;
		//c.weightx = 33;
		//c.weighty = 20;			
		
		JButton een = new JButton("Eenen");
		 eenl = new JLabel(speler.getPuntenVoorCategorieString(0));
		JButton twee = new JButton("twee�n");
		 tweel = new JLabel(speler.getPuntenVoorCategorieString(1));
		JButton drie = new JButton("drie�n");
		 driel = new JLabel(speler.getPuntenVoorCategorieString(2));
		JButton vier = new JButton("vieren");
		 vierl = new JLabel(speler.getPuntenVoorCategorieString(3));
		JButton vijf = new JButton("vijven");
		 vijfl = new JLabel(speler.getPuntenVoorCategorieString(4));
		JButton zes = new JButton("zessen");
		 zesl = new JLabel(speler.getPuntenVoorCategorieString(5));
		JButton dezelfde3 = new JButton("3 dezelfde");
		 dezelfde3l = new JLabel(speler.getPuntenVoorCategorieString(6));
		JButton dezelfde4 = new JButton("4 dezelfde");
		 dezelfde4l = new JLabel(speler.getPuntenVoorCategorieString(7));
		JButton fullhouse = new JButton("fullhouse");
		 fullhousel = new JLabel(speler.getPuntenVoorCategorieString(8));
		JButton kleine = new JButton("kleinestraat");
		 kleinel = new JLabel(speler.getPuntenVoorCategorieString(9));
		JButton grote = new JButton("grotestraat");
		 grotel = new JLabel(speler.getPuntenVoorCategorieString(10));
		JButton yatzee = new JButton("yatzee");
		 yatzeel = new JLabel(speler.getPuntenVoorCategorieString(11));
		JButton chance = new JButton("chance");
		 chancel = new JLabel(speler.getPuntenVoorCategorieString(12));
		
		JLabel uppertitle = new JLabel("naam sectie  ");
		punten.add(uppertitle,c);
		c.gridy =1;
		punten.add(een ,c);
		c.gridy =2;
		punten.add(twee,c);
		c.gridy =3;
		punten.add(drie,c);
		c.gridy =4;
		punten.add(vier,c);
		c.gridy =5;
		punten.add(vijf,c);	
		c.gridy =6;
		punten.add(zes,c);
		c.gridx = 1;
		c.gridy = 0;
		JLabel upperpunten = new JLabel("punten sectie   ");
		punten.add(upperpunten,c);
		c.gridy = 1;
		punten.add(eenl,c);
		c.gridy = 2;
		punten.add(tweel,c);
		c.gridy =3;
		punten.add(driel,c);
		c.gridy =4;
		punten.add(vierl,c);
		c.gridy =5;
		punten.add(vijfl,c);
		c.gridy =6;
		punten.add(zesl,c);
		
		c.gridx = 3;
		c.gridy = 0;
		JLabel lowernamen = new JLabel("naam sectie  ");
		punten.add(lowernamen,c);
		c.gridy = 1;
		punten.add(dezelfde3,c);
		c.gridy = 2;
		punten.add(dezelfde4,c);
		c.gridy = 3;
		punten.add(fullhouse,c);
		c.gridy = 4;
		punten.add(kleine,c);
		c.gridy = 5;
		punten.add(grote,c);
		c.gridy = 6;
		punten.add(yatzee,c);
		c.gridy = 7;
		punten.add(chance,c);
		c.gridy = 0;
		c.gridx = 4;
		
		
		JLabel lowerpunten = new JLabel("punten sectie   ");
		punten.add(lowerpunten,c);
		c.gridy = 1;
		punten.add(dezelfde3l,c);
		c.gridy = 2;
		punten.add(dezelfde4l,c);
		c.gridy = 3;
		punten.add(fullhousel,c);
		c.gridy = 4;
		punten.add(kleinel,c);
		c.gridy = 5;
		punten.add(grotel,c);
		c.gridy = 6;
		punten.add(yatzeel,c);
		c.gridy = 7;
		punten.add(chancel,c);
		
		een.setEnabled(false);
		twee.setEnabled(false);
		drie.setEnabled(false);
		vier.setEnabled(false);
		vijf.setEnabled(false);
		zes.setEnabled(false);
		dezelfde3.setEnabled(false);
		dezelfde4.setEnabled(false);
		fullhouse.setEnabled(false);
		kleine.setEnabled(false);
		grote.setEnabled(false);
		yatzee.setEnabled(false);
		chance.setEnabled(false);
		
		return punten;
	}
	public void nieuweScore() {
		eenl.setText(speler.getPuntenVoorCategorieString(0));
		tweel.setText(speler.getPuntenVoorCategorieString(1));
		driel.setText(speler.getPuntenVoorCategorieString(2));
		vierl.setText(speler.getPuntenVoorCategorieString(3));
		vijfl.setText(speler.getPuntenVoorCategorieString(4));
		zesl.setText(speler.getPuntenVoorCategorieString(5)); 
		dezelfde3l.setText(speler.getPuntenVoorCategorieString(6));
		dezelfde4l.setText(speler.getPuntenVoorCategorieString(7));
		fullhousel.setText(speler.getPuntenVoorCategorieString(8));
		kleinel.setText(speler.getPuntenVoorCategorieString(9));
		grotel.setText(speler.getPuntenVoorCategorieString(10));
		yatzeel.setText(speler.getPuntenVoorCategorieString(11));
		chancel.setText(speler.getPuntenVoorCategorieString(12));
		
	}

	@Override
	public void changeToPlayState() {
		gooi.setEnabled(true);
		speel.setEnabled(true);
		this.requestFocus();
	}
	@Override
	public void changeToWaiteState() {
		gooi.setEnabled(false);
		speel.setEnabled(false);
		
	}

}
