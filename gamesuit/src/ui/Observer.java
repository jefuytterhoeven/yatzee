package ui;

public interface Observer {

		public void updateDobbelstenen();
		
		public void updateScore();
		
		public void changeToPlayState();
		
		public void changeToWaiteState();
}
