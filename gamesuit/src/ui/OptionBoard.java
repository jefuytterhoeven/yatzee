package ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import ui.listeners.ButtonStartListener;
import ui.listeners.ButtonStopListener;
import ui.listeners.ButtonVoegSpelerToeScreenListener;
import ui.scoreObserver.UiScoreBoard;
public class OptionBoard extends JFrame{
	JButton startgame , voegtoe , stop;
	UiScoreBoard score;
	JPanel panel;
	public OptionBoard(){
		
	}
	public void toonVoorSpelScherm(ControllerYatzee controller){
		score = new UiScoreBoard(controller);
		controller.setScore(score);
		this.setSize(430,250);
		this.setLocationRelativeTo(null);	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("speloptie's");
		
		panel = new JPanel();
		
		startgame = new JButton("start het spel");
		voegtoe = new JButton("voeg spelers toe");
		stop = new JButton("stop het spel");
		
		ButtonStartListener buttonstartlistener = new ButtonStartListener(controller);
		startgame.addActionListener(buttonstartlistener);
		ButtonVoegSpelerToeScreenListener buttonvoegspelertoelistener = new ButtonVoegSpelerToeScreenListener(controller);
		voegtoe.addActionListener(buttonvoegspelertoelistener);
		ButtonStopListener buttonstoplistener = new ButtonStopListener(controller);
		stop.addActionListener(buttonstoplistener);
		
		panel.add(startgame);
		panel.add(voegtoe);
		panel.add(stop);
		
		
		this.add(panel);
		this.setVisible(true);
	}
	public void toonTijdensSpelScherm(ControllerYatzee controller){
		
	}
	public void close() {
		this.dispose();
		
	}
	public void changeToInGameState() {
		startgame.setEnabled(false);
		voegtoe.setEnabled(false);
		panel.add(score.drawScore());
		//panel.updateUI();
		
	}
}
