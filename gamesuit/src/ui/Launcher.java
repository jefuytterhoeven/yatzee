package ui;

import javax.swing.JOptionPane;

import exceptions.NogEenSpelExeption;

public class Launcher {
	public static void main(String[] args)
	{
		Object[] options = {"yatzee","zeeslag"};
		ControllerYatzee controller;
		int n = JOptionPane.showOptionDialog(null,
	            "kies alstublieft een spel",
	            "kies een spel",
	            JOptionPane.YES_NO_CANCEL_OPTION,
	            JOptionPane.DEFAULT_OPTION,
	            null,
	            options,
	            options[1]);  
		if(n== 0){
			boolean nog = true;
			while(nog == true){
				try{
					controller = new ControllerYatzee();
					controller.init();
					nog = false;
				}
				catch(NogEenSpelExeption e){
					
					if(e.getMessage().equals("speel nog eens")){
						nog = true;
					}
				}
			}
			
		}
		if(n == 1){
			JOptionPane.showMessageDialog(null, "zeeslag wer nog niet volledig geimplementeert");
			//controller = new ControllerZeeslag();
			//controller.init();
		}

	}
}
