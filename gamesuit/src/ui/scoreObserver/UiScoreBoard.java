package ui.scoreObserver;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import domain.model.Speler;
import ui.ControllerYatzee;

public class UiScoreBoard implements observer {
	ArrayList<Speler> spelers;
	ControllerYatzee controller;
	HashMap<String, JLabel> scores = new HashMap<>();
	JPanel score;
	public UiScoreBoard(ControllerYatzee controller) {
		this.controller = controller;
		this.addPlayers();
	}
	
	@Override
	public void updateScore() {
		for(int i = 0; i< spelers.size() ;i++){
			scores.get(spelers.get(i).getNaam()).setText(Integer.toString(spelers.get(i).getPuntenTotaal()));
		}
		
	}

	@Override
	public void addPlayers() {
		spelers = controller.getPlayers();
	}
	
	public JPanel drawScore() {
		score = new JPanel(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		
		JButton naamtitel = new JButton("naam van de speler");
		naamtitel.setEnabled(false);
		JLabel scoretitel = new JLabel("score van de speler");
		
		score.add(naamtitel,c);
		c.gridx = 1;
		score.add(scoretitel,c);
		
		for(int i = 0; i<spelers.size(); i++){
			c.gridx = 0;
			c.gridy++;
			JButton naam = new JButton(spelers.get(i).getNaam());
			JLabel scoreveld = new JLabel(Integer.toString(spelers.get(i).getPuntenTotaal()));
			scores.put(spelers.get(i).getNaam(), scoreveld);
			score.add(naam, c);
			c.gridx = 1;
			score.add(scoreveld, c);
		}
		
		return score;
	}
}









