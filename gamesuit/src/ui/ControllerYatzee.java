package ui;

import java.util.ArrayList;

import javax.swing.JTextField;

import domain.model.GameInfo;
import domain.model.Speler;
import domain.service.YatzeeFacade;
import ui.scoreObserver.UiScoreBoard;

public class ControllerYatzee implements Controller {
	private GetPlayersBoard getplayersboard;
	private OptionBoard optionboard;
	private YatzeeFacade facade;
	private UiScoreBoard score;
	public ControllerYatzee() {
		optionboard = new OptionBoard();
		getplayersboard = new GetPlayersBoard();
		facade = new YatzeeFacade();
		
	}
	@Override
	public void init() {
		facade.setGameInfo(new GameInfo());
		optionboard.toonVoorSpelScherm(this);	
	}
	public void closeAll() {
		if(getplayersboard != null){
			getplayersboard.Close();
		}
		for(int j = 0; j< facade.getPlayers().size() ;j++){
			facade.getPlayers().get(j).getPlayerBoard().dispose();
		}
		optionboard.close();
	}
	public void closeGetPlayersBoard(){
		if(getplayersboard != null){
			getplayersboard.Close();
		}
	}
	public void addPlayers(ArrayList<String> players) {
		facade.addPlayers(players , this);
		
	}
	public void toonVoegSpelerToeScherm() {
		getplayersboard.drawStartScreen(this);
		
	}
	@Override
	public void startSpel() {
		optionboard.changeToInGameState();
		for(int i = 0;i <facade.getPlayers().size();i++){
			facade.getPlayers().get(i).drawPlayerBoard();
		}
		this.changePlayer();
	}
	public void changePlayer(){
		facade.changePlayer();
	}
	public ArrayList<Speler> getPlayers(){
		return facade.getPlayers();
	}
	public void setScore(UiScoreBoard score){
		this.score = score;
	}
	public UiScoreBoard getScore(){
		return score;
	}



}
