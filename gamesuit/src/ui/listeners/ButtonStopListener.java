package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ui.ControllerYatzee;

public class ButtonStopListener implements ActionListener {
	private ControllerYatzee controller;
	public ButtonStopListener(ControllerYatzee controller) {
		this.controller = controller;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		controller.closeAll();
		
	}

}
