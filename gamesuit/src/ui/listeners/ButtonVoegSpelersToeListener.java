package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;

import ui.ControllerYatzee;

public class ButtonVoegSpelersToeListener implements ActionListener {
	private ControllerYatzee controller;
	private ArrayList<String> players = new ArrayList<>();
	private ArrayList<JTextField> textvelden;
	public ButtonVoegSpelersToeListener(ControllerYatzee controller, ArrayList<JTextField> players) {
		this.controller = controller;
		textvelden = players;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i< textvelden.size(); i++){
			if(textvelden.get(i).getText().equals("")){
				System.out.println("textveld was leeg en de gebruiker word niet toegevoegd");
			}else{
				this.players.add(textvelden.get(i).getText());
			}
		}
		controller.addPlayers(players);
		controller.closeGetPlayersBoard();
		
	}

}
