package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ui.PlayerBoard;

public class ButtonGooitDobbelstenenListener implements ActionListener  {
	PlayerBoard board;
	public ButtonGooitDobbelstenenListener(PlayerBoard playerBoard) {
		this.board = playerBoard;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		board.updateDobbelstenen();
	}

}
