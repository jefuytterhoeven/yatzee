package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ui.ControllerYatzee;

public class ButtonNietToevoegenListener implements ActionListener {
	private ControllerYatzee controller;
	public ButtonNietToevoegenListener(ControllerYatzee controller) {
		this.controller = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		controller.closeGetPlayersBoard();
		
	}

}
