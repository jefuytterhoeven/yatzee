package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTextField;

import ui.ControllerYatzee;

public class ButtonVoegSpelerToeScreenListener implements ActionListener {
	private ControllerYatzee controller;
	public ButtonVoegSpelerToeScreenListener(ControllerYatzee controller) {
		this.controller = controller;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		controller.toonVoegSpelerToeScherm();
		
	}

}
