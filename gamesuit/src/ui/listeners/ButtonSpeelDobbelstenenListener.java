package ui.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import exceptions.DomainException;
import ui.ControllerYatzee;
import ui.PlayerBoard;

public class ButtonSpeelDobbelstenenListener implements ActionListener {
	PlayerBoard board;
	ControllerYatzee controller;
	public ButtonSpeelDobbelstenenListener(PlayerBoard playerBoard, ControllerYatzee controller) {
		this.controller = controller;
		this.board = playerBoard;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try{
		board.updateScore();
		controller.changePlayer();
		controller.getScore().updateScore();
		}
		catch (DomainException es){
			JOptionPane.showMessageDialog(null ,es.getMessage());
		}
		
	}


}
