package ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import ui.listeners.ButtonNietToevoegenListener;
import ui.listeners.ButtonVoegSpelersToeListener;


public class GetPlayersBoard extends JFrame {
	JPanel getplayersboard;

	public GetPlayersBoard() {
		
	}
	
	public void drawStartScreen(ControllerYatzee controller)
	{
		this.setSize(430,250);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.setTitle("wie doet er mee?");
		JPanel panel = new JPanel();
		JLabel uitlegtext = new JLabel("geef voor elke speler een naam in een textveld. slechts 1 naam per veld\n");
		panel.add(uitlegtext);
		JTextField player1 = new JTextField("",15);
		JTextField player2 = new JTextField("",15);
		JTextField player3 = new JTextField("",15);
		JTextField player4 = new JTextField("",15);
		JTextField player5 = new JTextField("",15);
		JTextField player6 = new JTextField("",15);
		JTextField player7 = new JTextField("",15);
		JTextField player8 = new JTextField("",15);
		JTextField player9 = new JTextField("",15);
		JTextField player10 = new JTextField("",15);
		ArrayList<JTextField> players = new ArrayList<>();
		players.add(player1);
		players.add(player2);
		players.add(player3);
		players.add(player4);
		players.add(player5);
		players.add(player6);
		players.add(player7);
		players.add(player8);
		players.add(player9);
		players.add(player10);
		panel.add(player1);
		panel.add(player2);
		panel.add(player3);
		panel.add(player4);
		panel.add(player5);
		panel.add(player6);
		panel.add(player7);
		panel.add(player8);
		panel.add(player9);
		panel.add(player10);
		JButton startknop = new JButton("voeg spelers toe");
		JButton stopknop = new JButton("niet toevoegen");
		ButtonVoegSpelersToeListener buttonlistener = new ButtonVoegSpelersToeListener(controller, players);
		startknop.addActionListener(buttonlistener);
		ButtonNietToevoegenListener buttonstoplistener = new ButtonNietToevoegenListener(controller);
		stopknop.addActionListener(buttonstoplistener);
		panel.add(startknop);
		panel.add(stopknop);
		this.add(panel);
		this.setVisible(true);
		player1.requestFocus();

	}
	public void Close(){
		this.dispose();
	}

}
