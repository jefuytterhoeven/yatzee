package domain.service;

import java.util.ArrayList;

import domain.model.GameInfo;
import domain.model.Speler;
import ui.ControllerYatzee;
import ui.PlayerBoard;

public class YatzeeFacade {
	private GameInfo gameinfo;
	
	public void addPlayers(ArrayList<String> players , ControllerYatzee controller){
		gameinfo.addplayers(players, controller);
		
	}
	public void setGameInfo(GameInfo gameinfo){
		this.gameinfo = gameinfo;
	}
	public ArrayList<Speler> getPlayers(){
		return gameinfo.getPlayers();
	}
	public void changePlayer() {
		gameinfo.changeFocusedPlayer();
		
	}
}
