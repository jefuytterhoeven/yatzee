package domain.model;

public enum PuntenCategorie {
	eenen(0),twees(1),drie�n(2),vieren(3),vijven(4),zessen(5),driedezelfde(6),
	vierdezelfde(7),fullhouse(8),kleinestraat(9),grotestraat(10),yatzee(11),chance(12);
	private int locatie;
	
	private PuntenCategorie(int locatie){
		this.locatie = locatie;
	}
	public int getLocatie(){
		return locatie;
	}
}
