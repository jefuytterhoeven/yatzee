package domain.model;

import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class DiceFactory {
	
	public static JLabel makeDobbelsteen(){
		JLabel dobbelsteen;
		String url = "dobbelsteen/dobbelsteen1.jpg";
		String desc = "1";
		
		Random rand = new Random();
		int Low = 1;
		int High = 7;
		int result = rand.nextInt(High-Low) + Low;
		
		switch(result){
			case 1: url = "dobbelsteen/dobbelsteen1.jpg";
					desc ="1";
					System.out.println("dobbel1 is gemaakt");
				break;
			case 2: url = "dobbelsteen/dobbelsteen2.jpg";
			desc ="2";
			System.out.println("dobbel2 is gemaakt");
				break;
			case 3: url = "dobbelsteen/dobbelsteen3.jpg";
			desc ="3";
			System.out.println("dobbel3 is gemaakt");
				break;
			case 4: url = "dobbelsteen/dobbelsteen4.jpg";
			desc ="4";
			System.out.println("dobbel4 is gemaakt");
				break;
			case 5: url = "dobbelsteen/dobbelsteen5.jpg";
			desc ="5";
			System.out.println("dobbel5 is gemaakt");
				break;
			case 6: url = "dobbelsteen/dobbelsteen6.jpg";
			desc ="6";
			System.out.println("dobbel6 is gemaakt");
				break;
			
		}
		
		ImageIcon dob =new ImageIcon(url);
		Image img = dob.getImage();
		Image newimg = img.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		dob.setImage(newimg);
		
		dobbelsteen = new JLabel(dob);
		dobbelsteen.setToolTipText(desc);
		return dobbelsteen;
	}
	public static ImageIcon SetNewImage(){

		String url = "dobbelsteen/dobbelsteen1.jpg";
		String desc = "1";
		Random rand = new Random();
		int Low = 1;
		int High = 7;
		int result = rand.nextInt(High-Low) + Low;
		
		switch(result){
			case 1: url = "dobbelsteen/dobbelsteen1.jpg";
					desc= "1";
					System.out.println("dobbel1 is gemaakt");
				break;
			case 2: url = "dobbelsteen/dobbelsteen2.jpg";
			desc= "2";
			System.out.println("dobbel2 is gemaakt");
				break;
			case 3: url = "dobbelsteen/dobbelsteen3.jpg";
			desc= "3";
			System.out.println("dobbel3 is gemaakt");
				break;
			case 4: url = "dobbelsteen/dobbelsteen4.jpg";
			desc= "4";
			System.out.println("dobbel4 is gemaakt");
				break;
			case 5: url = "dobbelsteen/dobbelsteen5.jpg";
			desc= "5";
			System.out.println("dobbel5 is gemaakt");
				break;
			case 6: url = "dobbelsteen/dobbelsteen6.jpg";
			desc= "6";
			System.out.println("dobbel6 is gemaakt");
				break;
			
		}
		
		ImageIcon dob =new ImageIcon(url);
		Image img = dob.getImage();
		Image newimg = img.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		dob.setImage(newimg);
		dob.setDescription(desc);

		
		return dob;
		
	}


}
