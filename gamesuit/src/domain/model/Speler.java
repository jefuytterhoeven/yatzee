package domain.model;

import java.util.ArrayList;

import exceptions.DomainException;
import ui.ControllerYatzee;
import ui.PlayerBoard;
import ui.scoreObserver.UiScoreBoard;

public class Speler {
	boolean finished = false;
	String naam;
	ArrayList<Integer> punten = new ArrayList<>() ;
	PlayerBoard spelbord ;
	private ControllerYatzee controller;
	private UiScoreBoard score;
	
	public Speler(String naam , ControllerYatzee controller) {
		score = controller.getScore();
		this.controller = controller;
		spelbord = new PlayerBoard(this , controller);
		this.naam = naam;
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);
		punten.add(-1);

	}
	private void updateScore() {
		score.updateScore();
	}
	public Integer getPuntenVoorCategorie(Integer categorie){
		return punten.get(categorie);
	}
	public boolean getFinished(){
		return finished;
	}
	public String getPuntenVoorCategorieString(int categorie){
		return punten.get(categorie).toString();
	}
	public void drawPlayerBoard(){
		spelbord.drawPlayerBoard();
	}
	public Integer getPuntenTotaal(){
		Integer tot = 0;
		for(int i = 0; i<punten.size() ; i ++){
			if(punten.get(i) > -1){
				tot = tot + punten.get(i);
			}
		}
		return tot;
	}
	public void setNaam(String naam){
		this.naam = naam;
	}
	public String getNaam(){
		return naam;
	}
	public PlayerBoard getPlayerBoard(){
		return spelbord;
	}
	public void closeSpelbord(){
		spelbord.dispose();
	}
	private void isFinished(){
		finished = true;
		for(int i =0; i < punten.size();i++){
			if(punten.get(i) == -1){
				finished = false;
			}
		}
	}
	public void berekenScore(ArrayList<String> dobbelTooltip, PuntenCategorie puntenCategorie) {
		String cat = puntenCategorie.toString();
		int count = 0;
		ArrayList<Integer> dobbels = new ArrayList<>();
		System.out.println("de gekozen categorie is " + cat);
		
		for(int i = 0; i< dobbelTooltip.size(); i++){
			dobbels.add(Integer.parseInt(dobbelTooltip.get(i)));
		}
		switch(cat){
			case "eenen" :
					if(punten.get(0) != -1){
						throw new DomainException("deze categorie hebd u reeds gebruikt");
					}
					 count = 0;
					for(int i = 0 ; i< dobbelTooltip.size();i++){
						if(dobbelTooltip.get(i).equals("1")){
							count++;
						}
					}
					punten.set(0, count);
				break;
			case "twees" :
				if(punten.get(1) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(dobbelTooltip.get(i).equals("2")){
						count++;
					}
				}
				punten.set(1, count*2);
				break;
			case "drie�n" :
				if(punten.get(2) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(dobbelTooltip.get(i).equals("3")){
						count++;
					}
				}
				punten.set(2, count*3);
				break;
			case "vieren" :
				if(punten.get(3) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(dobbelTooltip.get(i).equals("4")){
						count++;
					}
				}
				punten.set(3, count*4);
				break;
			case "vijven" :
				if(punten.get(4) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(dobbelTooltip.get(i).equals("5")){
						count++;
					}
				}
				punten.set(4, count*5);
				break;
			case "zessen" :
				if(punten.get(5) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(dobbelTooltip.get(i).equals("6")){
						count++;
					}
				}
				punten.set(5, count*6);
				break;
			case "driedezelfde" :
				if(punten.get(6) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				for(int j =1 ; j<=7 ;j++){
					count = 0;
					for(int i = 0 ; i< dobbelTooltip.size();i++){
						if(Integer.parseInt(dobbelTooltip.get(i)) == j){
							count++;
						}
					}
					if(count >= 3){
						count = 0;
						for(int i = 0 ; i< dobbelTooltip.size();i++){
							count = count + Integer.parseInt(dobbelTooltip.get(i));
						}
						
					}
					punten.set(6, count);
				}
				
				break;
			case "vierdezelfde" :
				if(punten.get(7) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				for(int j =1 ; j<=7 ;j++){
					count = 0;
					for(int i = 0 ; i< dobbelTooltip.size();i++){
						if(Integer.parseInt(dobbelTooltip.get(i)) == j){
							count++;
						}
					}
					if(count >= 4){
						count = 0;
						for(int i = 0 ; i< dobbelTooltip.size();i++){
							count = count + Integer.parseInt(dobbelTooltip.get(i));
						}
						
					}
					punten.set(7, count);
				}
				break;
			case "fullhouse" :
				if(punten.get(8) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				int ee = 0;
				
				for(int j =1 ; j<=7 ;j++){
					count = 0;
					for(int i = 0 ; i< dobbelTooltip.size();i++){
						if(Integer.parseInt(dobbelTooltip.get(i)) == j){
							count++;
						}
					}
					if(count >= 3){
						ee = j;
					}
					
				}
				
				for(int j =1 ; j<=7 ;j++){
					count = 0;
					for(int i = 0 ; i< dobbelTooltip.size();i++){
						if(Integer.parseInt(dobbelTooltip.get(i)) == j){
							if(Integer.parseInt(dobbelTooltip.get(i)) != ee){
								count++;
							}
						}
					}
					if(count >= 2){
						count = 0;
						for(int i = 0 ; i< dobbelTooltip.size();i++){
							count = count + Integer.parseInt(dobbelTooltip.get(i));
						}
						
					}
					punten.set(8, count);
				}
				break;
			case "kleinestraat" :
				if(punten.get(9) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 boolean[] valueinHand = new boolean[6];
				 
				 	for (int i =0;i<dobbelTooltip.size();i++) {
				        valueinHand[Integer.parseInt(dobbelTooltip.get(i)) -1] = true;
				    }
				 
					for (boolean value : valueinHand) {
				        if (value == true) {
				            count++;
				        } else {
				            count = 0;
				        }
				        // works for any number of cards
				        if (count >= 4) {
				        	punten.set(9, 30);
				        }else{
				        	punten.set(9, 0);
				        }
				    }
				break;
			case "grotestraat" :
				if(punten.get(10) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				 boolean[] valueInHand = new boolean[6];
				 
				 	for (int i =0;i<dobbelTooltip.size();i++) {
				        valueInHand[Integer.parseInt(dobbelTooltip.get(i)) -1] = true;
				    }
				 
					for (boolean value : valueInHand) {
				        if (value == true) {
				            count++;
				        } else {
				            count = 0;
				        }
				        // works for any number of cards
				        if (count <= 5) {
				        	punten.set(10, 40);
				        }else{
				        	punten.set(10, 0);
				        }
				    }
				
				break;
			case "yatzee" :
				if(punten.get(11) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					if(Integer.parseInt(dobbelTooltip.get(i)) != Integer.parseInt(dobbelTooltip.get(1))){
						count = 1;
					}
				}
				if(count == 0){
					punten.set(11, 50);
				}else{
		        	punten.set(11, 0);
		        }
				
				break;
			case "chance" :
				if(punten.get(12) != -1){
					throw new DomainException("deze categorie hebd u reeds gebruikt");
				}
				count = 0;
				for(int i = 0 ; i< dobbelTooltip.size();i++){
					count = count + Integer.parseInt(dobbelTooltip.get(i));
				}
				punten.set(12, count);
				break;
		}
		isFinished();
		spelbord.nieuweScore();
		this.updateScore();
		
		
	}

}
