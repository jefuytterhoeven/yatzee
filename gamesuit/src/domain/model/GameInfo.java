package domain.model;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import exceptions.NogEenSpelExeption;
import ui.ControllerYatzee;


public class GameInfo {
	ArrayList<Speler> players = new ArrayList<>();
	int focusedplayer = 0;
	public void addplayers(ArrayList<String> players , ControllerYatzee controller) {
		Speler nieuwespeler;
		
		for(int i =0 ; i < players.size();i++){
			nieuwespeler = new Speler(players.get(i), controller);
			this.players.add(nieuwespeler);
			System.out.println(nieuwespeler.getNaam());
		}
		
	}
	public void changeFocusedPlayer(){
		
		players.get(focusedplayer).getPlayerBoard().changeToWaiteState();
		if(focusedplayer < players.size() - 1){
			focusedplayer++;
		}else{
			focusedplayer = 0;
		}
		if(players.get(focusedplayer).getFinished() == false){
			players.get(focusedplayer).getPlayerBoard().changeToPlayState();
		}else{
			gameFinished();
		}
		
	}

	private void gameFinished() {
		int hoscore = 0;
		String honaam = null;
		
		for(int i =0 ; i < players.size();i++){
			if(players.get(i).getPuntenTotaal() > hoscore){
				hoscore = players.get(i).getPuntenTotaal();
				honaam = players.get(i).getNaam();
			}
			players.get(i).closeSpelbord();
		}
		JOptionPane.showMessageDialog(null, honaam +" heeft gewonnen met" + hoscore +" punten, proficiat" + honaam);
		int nogeens = JOptionPane.showConfirmDialog(null, "wilt u nog eens spelen ?");
		if(nogeens == JOptionPane.YES_OPTION ){
			throw new NogEenSpelExeption("speel nog eens");
		}
	}
	public ArrayList<Speler> getPlayers() {
		// TODO Auto-generated method stub
		return players;
	}
}
