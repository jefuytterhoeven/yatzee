package exceptions;

public class NogEenSpelExeption extends RuntimeException {

	public NogEenSpelExeption(Exception e){
		super(e);
	}
	public NogEenSpelExeption(String e){
		super(e);
	}
	
	public NogEenSpelExeption(String e, Exception ex)
	{
		super(e, ex);
	}
}
