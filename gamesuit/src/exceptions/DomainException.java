package exceptions;

public class DomainException extends RuntimeException  {
	
	public DomainException(Exception e){
		super(e);
	}
	public DomainException(String e){
		super(e);
	}
	
	public DomainException(String e, Exception ex)
	{
		super(e, ex);
	}
}

